using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{
	#region CONSTANTS

	#endregion

	#region STATIC_MEMBERS

	private static GameManager sInstance;

	#endregion

	#region PRIVATE_MEMBERS

	float mScore;
	float mScoreMultiplier = 15;
	Transform mPlayerTransform;
	float mLastY;

	#endregion

	#region ACCESSORS

	public static GameManager Instance
	{
		get {return sInstance;}
	}

	public Transform PlayerTransform
	{
		set
		{
			mPlayerTransform = value;
			mLastY = mPlayerTransform.position.y;
		}
	}

	public int Score
	{
		get{return (int)mScore;}
	}

	#endregion

	#region MONO_METHODS

	void Awake()
	{
		if (sInstance == null)
		{
			sInstance = this;
		}
		else
		{
			Debug.Log("There is already an instance of the GameManager in the scene.");
		}
	}

	void Update()
	{
		if(mPlayerTransform != null && mPlayerTransform.position.y > mLastY)
		{
			mScore += (mPlayerTransform.position.y-mLastY)*mScoreMultiplier;

			mLastY = mPlayerTransform.position.y;
		}
	}

	void OnGUI()
	{
		GUI.Label(new Rect(10,10,80,30),"Score : "+(int)mScore);
	}

	#endregion

	#region PUBLIC_METHODS

	public void Reset()
	{
		mScore = 0;
		mPlayerTransform = null;
	}

	#endregion

	#region PRIVATE_METHODS

	#endregion
}
