using UnityEngine;
using System.Collections;

public class UIManager : MonoBehaviour
{
	#region CONSTANTS

	#endregion

	#region STATIC_MEMBERS

	private static UIManager sInstance;

	#endregion

	#region PRIVATE_MEMBERS

	#endregion

	#region ACCESSORS

	public static UIManager Instance
	{
		get {return sInstance;}
	}

	#endregion

	#region MONO_METHODS

	void Awake()
	{
		if (sInstance == null)
		{
			sInstance = this;
		}
		else
		{
			Debug.Log("There is already an instance of the UIManager in the scene.");
		}
	}

	#endregion

	#region PUBLIC_METHODS

	public void LoadScene(EScene aScene)
	{
		Application.LoadLevel(aScene.ToString());
	}
	 

	#endregion

	#region PRIVATE_METHODS

	#endregion
}
