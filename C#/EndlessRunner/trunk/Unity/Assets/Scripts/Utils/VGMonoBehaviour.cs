﻿using UnityEngine;
using System.Collections;

public class VGMonoBehavior : MonoBehaviour {
	
	#region PROTECTED_MEMBERS
	
	protected Transform mTransform;
	protected GameObject mGameObject;
	protected Rigidbody2D mRigidbody2D;
	protected Collider2D mCollider2D;
	
	#endregion
	
	#region ACCESSORS
	
	public Transform Transform
	{
		get{return mTransform;}
	}
	
	public GameObject GameObject
	{
		get{return mGameObject;}
	}
	
	public Vector3 TransformPosition
	{
		get{return mTransform.position;}
		set
		{
			mTransform.position = value;
		}
	}
	
	public Vector3 LocalTransformPosition
	{
		get{return mTransform.localPosition;}
		set{mTransform.localPosition = value;}
	}
	
	public Collider2D Collider2D
	{
		get{return mCollider2D;}
	}
	
	public Rigidbody2D Rigidbody2D
	{
		get{return mRigidbody2D;}
	}
	
	#endregion
	
	#region MONO_METHODS
	
	protected virtual void Awake()
	{
		mTransform 			= transform;
		mGameObject 		= gameObject;
		mRigidbody2D 		= rigidbody2D;
		mCollider2D 		= collider2D;
	}
	
	#endregion
}
