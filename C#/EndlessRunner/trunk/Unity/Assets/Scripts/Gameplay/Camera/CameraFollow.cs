﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	public Transform mFollowed;
	float mLastY = 0;

	// Update is called once per frame
	void Update () {
		if (mFollowed.position.y > mLastY)
		{
			mLastY = mFollowed.position.y;
			transform.position = new Vector3(transform.position.x,mFollowed.transform.position.y,transform.position.z);
		}
	}
}
