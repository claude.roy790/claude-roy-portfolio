﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer))][RequireComponent(typeof(BoxCollider2D))]
public abstract class BaseCollectable : VGMonoBehavior {

	#region PUBLIC_MEMBERS

	public AudioClip mCollectSound;

	#endregion

	#region MONO_METHODS

	protected override void Awake ()
	{
		base.Awake ();

		//
		mCollider2D.isTrigger = true;
	}
	
	protected virtual void OnTriggerEnter2D(Collider2D aCol)
	{
		/*CharacterMovementAndJump m = aCol.GetComponent<CharacterMovementAndJump>();
		
		if (m != null)
		{
			Collect();
		}*/
	}
	
	#endregion
	
	#region PROTECTED_METHODS
	
	public virtual void Collect()
	{
		if (mCollectSound != null)
		{
			AudioSource.PlayClipAtPoint(mCollectSound,Vector2.zero);
		}

		Destroy(GameObject);
	}
	
	#endregion
}
