﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlatformSpawner : VGMonoBehavior {

	public GameObject mPlatformPrefab;
	public Transform mPlayerTransform;
	public Transform mPlatformsParent;
	public int mNbMinPlatformPerRow = 2;
	public int mNbMaxPlatformPerRow = 5;
	public float mHeightDistanceBetweenSpawn =1f;
	public float minHorizontalDistanceBetweenPlatforms = 0.2f;

	private Camera mCamera;
	private float mSpawnMinX = 0;
	private float mSpawnMaxX = 0;
	private float mDistanceBetweenPlatform = 0;
	private float mLastYSpawnPosition = int.MinValue;
	private Vector2 platformSize;
	private int mMaxNbPlatforms;
	List<float> mPlatformsOnSameRowXPos;

	protected override void Awake()
	{
		base.Awake();
		mPlatformsOnSameRowXPos = new List<float>();
		mCamera = Camera.main; 

		//Set x pos
		Vector3 leftWorldPos = mCamera.ViewportToWorldPoint(new Vector3(0,0,0));
		Vector3 rightWorldPos = mCamera.ViewportToWorldPoint(new Vector3(1,0,0));
		Vector3 topWorldPos = mCamera.ViewportToWorldPoint(new Vector3(0,1,0));

		//
		mMaxNbPlatforms = (int)((rightWorldPos.x-leftWorldPos.x)/mNbMaxPlatformPerRow);

		//
		mTransform.position = new Vector3(leftWorldPos.x -mTransform.localScale.x/2,topWorldPos.y+mTransform.localScale.y/2,mTransform.position.z);

		//Set max and min span positions
		platformSize = mPlatformPrefab.gameObject.transform.GetComponent<BoxCollider2D>().size;
		mSpawnMinX = leftWorldPos.x + platformSize.x/2;
		mSpawnMaxX = rightWorldPos.x - platformSize.x/2;
	
		mDistanceBetweenPlatform = platformSize.x;

		if ((rightWorldPos.x-leftWorldPos.x)/mDistanceBetweenPlatform < mNbMaxPlatformPerRow)
		{
			int size = (int)(rightWorldPos.x-leftWorldPos.x)/mNbMaxPlatformPerRow;
			mNbMaxPlatformPerRow = size;
			Debug.Log("Nb Max of platforms per row was too high, setting it to : "+size);
		}

		//Spawn initial Platforms
		SpawnPlatformInRange(0,topWorldPos.y);
	}
	
	void Start()
	{
		//
		InitiallySpawnPlatforms();
	}

	void Update()
	{
		if (mTransform.position.y - mLastYSpawnPosition > mHeightDistanceBetweenSpawn)
		{
			SpawnPlatformsInRow(mTransform.position.y);
		}
	}

	#region PLATFORMS_SPAWNING

	void SpawnPlatformInRange(float aStartY, float aEndY)
	{
		//
		aStartY = Mathf.Clamp(aStartY,int.MinValue,aEndY);

		float y = aStartY;

		while(y < aEndY)
		{
			SpawnPlatformsInRow(y);

			y+= mHeightDistanceBetweenSpawn;
		}
	}

	void SpawnPlatformsInRow(float aY)
	{
		mPlatformsOnSameRowXPos.Clear();

		mLastYSpawnPosition = aY;

		int maxFittingNbOfPlatformsForWidth = GetNbMaxFittingPlatforms();

		//
		if (maxFittingNbOfPlatformsForWidth > mNbMinPlatformPerRow)
		{
			mNbMinPlatformPerRow = maxFittingNbOfPlatformsForWidth;
		}

		int nb = Random.Range(mNbMinPlatformPerRow,maxFittingNbOfPlatformsForWidth+1);

		for(int i = 0;i < nb;i++)
		{
			bool posValid = false;
			Vector2 pos = Vector2.zero;

			while (!posValid)
			{
				pos = new Vector2(Random.Range(mSpawnMinX,mSpawnMaxX),aY);

				posValid = IsSpawnPositionValid(pos.x);

				if (posValid)
				{
					SpawnPlatform(pos);
				}
			}
		}
	}

	void SpawnPlatform(Vector2 aPosition)
	{
		GameObject go = Instantiate(mPlatformPrefab) as GameObject;
		go.transform.parent = mPlatformsParent;
		go.transform.position = aPosition;
		go.collider2D.enabled = false;
		//TODO Remove -CR.
		go.GetComponent<SpriteRenderer>().color = Color.blue;
		mPlatformsOnSameRowXPos.Add(go.transform.position.x);
		PlatformsManager.Instance.AddPlatform(go.transform);
	}

	void InitiallySpawnPlatforms()
	{

	}

	bool IsSpawnPositionValid(float aPosition)
	{
		bool returnValue = true;
		float platformSize = (mPlatformPrefab.collider2D as BoxCollider2D).size.x;

		for(int i =0;i <mPlatformsOnSameRowXPos.Count;i++)
		{
			if (Mathf.Abs(aPosition-mPlatformsOnSameRowXPos[i]) < platformSize+minHorizontalDistanceBetweenPlatforms)
			{
				returnValue = false;
				break;
			}
		}

		return returnValue;
	}

	#endregion

	#region UTILS_METHODS

	int GetNbMaxFittingPlatforms()
	{
		int returnValue = mMaxNbPlatforms;
		float maxDist = mSpawnMaxX - mSpawnMinX;
		float platformWidth =(mPlatformPrefab.collider2D as BoxCollider2D).size.x;
		float dist = float.MaxValue;

		//TODO Temp test -CR.
		int i = 0;
		while (dist > maxDist && i < 150)
		{
			dist = returnValue*platformWidth+(returnValue-1)*mDistanceBetweenPlatform;
			returnValue--;
			i++;
		}

		if (i == 150)
		{
			Debug.LogError("WOULD HAVE CRASHED");
			Debug.Break();
		}

		return returnValue;
	}

	#endregion
}
