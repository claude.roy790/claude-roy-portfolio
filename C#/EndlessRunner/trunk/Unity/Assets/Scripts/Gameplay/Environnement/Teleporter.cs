﻿using UnityEngine;
using System.Collections;

public class Teleporter : VGMonoBehavior {

	public Teleporter mTeleportLocation;
	public bool mIsAtLeftOfScreen = false;
	public CharacterTilt mTilt  = null;
	
	Camera mCamera;

	void Start()
	{
		mCamera = Camera.main;

		BoxCollider2D col = mCollider2D as BoxCollider2D;

		if (mIsAtLeftOfScreen)
		{
			Vector3 worldPos = mCamera.ViewportToWorldPoint(new Vector3(0,0,0));
			mTransform.position = new Vector3(worldPos.x -col.size.x/2,mTransform.position.y,mTransform.position.z);
		}
		else
		{
			Vector3 worldPos = mCamera.ViewportToWorldPoint(new Vector3(1,0,0));
			mTransform.position = new Vector3(worldPos.x+col.size.x/2,mTransform.position.y,mTransform.position.z);
		}
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player")
		{
			if (mTeleportLocation != null)
			{
				if ((mTilt.IsFacingRight && !mIsAtLeftOfScreen) || (!mTilt.IsFacingRight && mIsAtLeftOfScreen))
				{
					col.transform.position = new Vector3(mTeleportLocation.transform.position.x,
					                                   col.transform.position.y,col.transform.position.z);
				}
			}
			else
			{
				Debug.LogError("Could not teleport, the location is null");
			}
		}
	}
}
