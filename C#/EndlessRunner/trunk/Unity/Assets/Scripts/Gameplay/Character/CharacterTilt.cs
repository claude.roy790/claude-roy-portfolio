﻿using UnityEngine;
using System.Collections;

public class CharacterTilt : VGMonoBehavior {
	
	public float mMoveSpeed = 10;

	bool facingRight = true;
	float flipThreshold = 0.05f;

	public bool IsFacingRight
	{
		get{return facingRight;}
	}

	protected override void Awake()
	{
		base.Awake();
		facingRight = mTransform.localScale.x  == 1;
	}


	//
	void FixedUpdate()
	{
		mRigidbody2D.velocity = new Vector2(CrossPlatformInput.GetHorizontalAxis()*mMoveSpeed,mRigidbody2D.velocity.y);

		if (CrossPlatformInput.GetHorizontalAxis() > flipThreshold && !facingRight)
		{
			Flip();
		}
		else if (CrossPlatformInput.GetHorizontalAxis() < flipThreshold*-1 && facingRight)
		{
			Flip();
		}
	}

	void Flip()
	{
		Vector3 scale = mTransform.localScale;

		scale.x *= -1;

		mTransform.localScale = scale;

		facingRight = !facingRight;
	}
}
