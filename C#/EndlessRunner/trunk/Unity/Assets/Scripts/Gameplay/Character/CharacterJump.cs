﻿using UnityEngine;
using System.Collections;

public class CharacterJump : VGMonoBehavior {

	public Transform mGroundCheck;
	public float mJumpForce = 700f;
	public LayerMask mJumpOnLayer;
	//
	bool mIsGrounded;
	BoxCollider2D mBoxCollider2D;

	protected override void Awake()
	{
		base.Awake();

		mBoxCollider2D = mCollider2D as BoxCollider2D;

		if (GameManager.Instance)
		{
			//TODO Change code to move obvious place. -CR
			GameManager.Instance.PlayerTransform = mTransform;
			PlatformsManager.Instance.PlayerTransform = mTransform;
		}
	}

	void FixedUpdate()
	{
		//
		UpdateGrounded();

		//If im grounded and im going down.
		if (mIsGrounded && mRigidbody2D.velocity.y <= 0)
		{
			Jump();
		}
	}

	void Jump()
	{
		mRigidbody2D.velocity = new Vector2(mRigidbody2D.velocity.x,0);
		mRigidbody2D.AddForce(new Vector2(0,mJumpForce));
	}

	void UpdateGrounded()
	{
		Debug.DrawLine(mTransform.position,mGroundCheck.position,Color.green);

		//Center
		mIsGrounded = Physics2D.Linecast(mTransform.position,mGroundCheck.position,mJumpOnLayer.value);

		//Left
		if (!mIsGrounded)
		{
			Vector2 start = new Vector2(mTransform.position.x - mBoxCollider2D.size.x/2,mTransform.position.y);
			Vector2 end = new Vector2(mGroundCheck.position.x - mBoxCollider2D.size.x/2,mGroundCheck.position.y);
			mIsGrounded = Physics2D.Linecast(start,end,mJumpOnLayer.value);
		}

		//Right
		if (!mIsGrounded)
		{
			Vector2 start = new Vector2(mTransform.position.x + mBoxCollider2D.size.x/2,mTransform.position.y);
			Vector2 end = new Vector2(mGroundCheck.position.x + mBoxCollider2D.size.x/2,mGroundCheck.position.y);
			mIsGrounded = Physics2D.Linecast(start,end,mJumpOnLayer.value);
		}
	}
}
