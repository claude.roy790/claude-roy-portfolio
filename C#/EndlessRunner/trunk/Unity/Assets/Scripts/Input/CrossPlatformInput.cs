﻿using UnityEngine;
using System.Collections;

public static class CrossPlatformInput {

	public static float GetHorizontalAxis()
	{
		if (Application.isEditor)
		{
			return Input.GetAxis("Horizontal");
		}
		else
		{
			return Input.acceleration.x;
		}
	}
}
