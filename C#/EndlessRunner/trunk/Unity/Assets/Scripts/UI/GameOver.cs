﻿using UnityEngine;
using System.Collections;

public class GameOver : MonoBehaviour {

	public TextMesh text;

	// Use this for initialization
	void Start () {
		if (GameManager.Instance)
			text.text = "Score : "+GameManager.Instance.Score;
	}
	
	// Update is called once per frame
	void OnGUI()
	{
		if (GUI.Button(new Rect(10,10,Screen.width*0.3f,Screen.height*0.3f),"Restart"))
		{
			if (GameManager.Instance)
			{
				GameManager.Instance.Reset();
				PlatformsManager.Instance.Reset();
				Application.LoadLevel(1);
			}
			else
			{
				Application.LoadLevel(0);
			}
		}
	}
}
