﻿using UnityEngine;
using UnityEditor;
using System.Collections;


public class AddChildMenuOption : MonoBehaviour {

	[MenuItem("GameObject/Create Empty Child #&n")]
	static void CreateChild() {
		if(Selection.activeTransform != null)
		{
			GameObject go = new GameObject("GameObject");
			go.transform.parent = Selection.activeTransform;
			go.transform.localPosition = Vector3.zero;
			Selection.activeTransform = go.transform;
		}
		
	}
	
}