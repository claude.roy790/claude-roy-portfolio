## Welcome to Claude Roy's Portfolio

Here are a few projects that I've done using multiple technologies

## C# Projects.

Here I showcase video games that I've done in C# with the Unity engine (www.unity3d.com)

## Web Projects.

Here are web projects that I've programmed for myself in multiple technologies

## Browsing the Portfolio

To be able too see the projects, you can go in the Repository tab and explor the project right there. Here's a direct link to the Repositry tab if you can't find it: https://gitlab.com/claude.roy790/claude-roy-portfolio/tree/master

You can also clone the repository with git without any credentials via this url: https://gitlab.com/claude.roy790/claude-roy-portfolio.git

## Contact

If you have any questions/comments about the projects, you can contact me via email at claude.roy790@gmail.com